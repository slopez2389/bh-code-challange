## Environment Setup Instructions

- To set up project please run the following commands:
    - `npm run setup-local`
    - `npm start` A new browser window should open. If one does not open please visit [http://localhost:3000](http://localhost:3000).
    
    
### Backend

- The following endpoints have been set up
  - `/signup` to create a user
  - `/login` to log in an existing user
  - `/logout` to log out a logged in user
  - `/me` to retrieve the logged in user's profile
  - `/users` to retrieve a list of registered users. Instructions on how to set up an **admin user** can be found at the bottom.
  
  
### Frontend

- The following routes have been set up
    - `/` this is the home route. You should be able to sign in/register new user.
    - `/profile` will retrieve the logged in user's profile.
    -  `/users` will retrieve a list of created users for an admin user. 
    - `/logout` to logout user
  
  
#### Create Admin User

- please run the following command to create an admin user.
    - `CREATE_ADMIN_USER=true npm start`. Be sure to copy the generated password so that you may use it to test. **Scroll above webpack logs in terminal to see info.**


