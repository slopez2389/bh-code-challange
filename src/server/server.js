/* eslint-disable no-console */
import express from 'express';
import webpack from 'webpack';
import path from 'path';
import bodyParser from 'body-parser';
import dovenv from 'dotenv';
import open from 'open';
import webpackConfig from '../../webpack.config.dev';
import UserModel from './db/UserModel';
import { createTokenWithUser, formatResponseError } from './utils';
import protectedRoute from './middlewares/protectedRoute';
import requiresRole from './middlewares/requiresRole';
import createAdminUser from '../scripts/createAdminUser';

dovenv.load();

const app = express();
const compiler = webpack(webpackConfig);
const { PORT, CREATE_ADMIN_USER } = process.env;

// create admin user
if (CREATE_ADMIN_USER) {
  createAdminUser(UserModel);
}

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: webpackConfig.output.publicPath,
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/signup', async (req, res) => {
  try {
    const { name, email, password } = req.body;
    const lowerCaseEmail = email.toLowerCase();
    const NewUser = new UserModel({ name, email: lowerCaseEmail, password });
    const createdUser = await NewUser.save();

    if (createdUser._id) {
      const token = createTokenWithUser(createdUser);

      res.json({ success: true, token });
    }
  } catch ({ errors, message }) {
    res.status(400).json({ errors, message });
  }
});

app.post('/login', async (req, res) => {
  const invalidCredentialsMsg = 'Incorrect email or password';
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      res.status(400).json(formatResponseError('email and password are required'));
    } else {
      const lowerCaseEmail = email.toLowerCase();
      const dbUser = await UserModel.findOne({ email: lowerCaseEmail });
      const isPasswordMatch = await UserModel.comparePassword(password, dbUser.password);

      if (!dbUser || !isPasswordMatch) {
        res.status(400).json(formatResponseError(invalidCredentialsMsg));
      }

      const token = createTokenWithUser(dbUser);

      res.json({ success: true, token });
    }
  } catch (error) {
    res.status(400).json(formatResponseError(invalidCredentialsMsg));
  }
});

app.post('/me', protectedRoute, async (req, res) => {
  try {
    if (req.user) {
      const dbUser = await UserModel.getUserById(req.user.user_id);
      if (dbUser) {
        res.json({
          success: true,
          name: dbUser.name,
          email: dbUser.email,
        });
      } else {
        res.status(401).json(formatResponseError('User not found!'));
      }
    }
  } catch (error) {
    res.status(401).json(formatResponseError(error.message));
  }
});

// this is just to simulate logging out a user
// the token is being deleted on the client
app.post('/logout', (req, res) => {
  res.json({
    success: false,
    token: null,
  });
});

app.post('/users', [protectedRoute, requiresRole('admin')], async (req, res) => {
  // define what fields to return from document
  const projection = { email: 1, name: 1, _id: 0 };
  const dbUsers = await UserModel.find({}, projection);
  res.json({ users: dbUsers });
});

// serve static assets
app.use(express.static(path.resolve(path.join(__dirname, '../public'))));

// route all other requests to index.html
app.use('*', (req, res) => {
  res.sendFile(path.resolve(path.join(__dirname, '../public', 'index.html')));
});

app.listen(PORT, (err) => {
  if (err) {
    console.error(err.message);
  } else {
    open(`http://localhost:${PORT}`);
  }
});
