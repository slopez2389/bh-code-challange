import { decodeToken, formatResponseError } from '../utils';

export default function (req, res, next) {
  const { accessToken } = req.body;
  const invalidTokenMessage = 'Please register and/or login and try again.';
  if (accessToken) {
    try {
      // all good add user to req for use in other routes/middleware's
      req.user = decodeToken(accessToken);
      next();
    } catch (err) {
      res.status(401).json(formatResponseError(invalidTokenMessage));
    }
  } else {
    res.status(401).json(formatResponseError(invalidTokenMessage));
  }
}
