export default function (role) {
  return function (req, res, next) {
    const isAllowed = req.user.user_role === role;
    if (!isAllowed) {
      res.status(403).json({
        success: false,
        message: 'Not authorized to access this resource',
      });
    } else {
      next();
    }
  };
}
