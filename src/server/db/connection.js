/* eslint-disable no-console */
import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.load();

// use native promises related to mongoose promise deprecation warning.
// more info here https://stackoverflow.com/questions/38138445/node3341-deprecationwarning-mongoose-mpromise
mongoose.Promise = global.Promise;

export default mongoose.connect(process.env.MONGO_URL, { useMongoClient: true }, (err) => {
  if (err) {
    console.error('error connecting to mongo', err.message);
  } else {
    console.log('successfully connected to mongo');
  }
});
