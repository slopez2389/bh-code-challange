/* eslint-disable no-useless-escape */
/* eslint-disable no-console */
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import db from './connection';

const emailRegex = /^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

// User schema definition
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required'],
  },
  email: {
    type: String,
    validate: {
      validator: (email) => emailRegex.test(email),
      message: '{VALUE} is an Invalid Email!',
    },
    required: [true, 'Email is required'],
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user',
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
  },
});

// check if email already exists
UserSchema.pre('validate', function (next) {
  this.constructor.findOne({ email: this.email }, (err, foundUser) => {
    if (err) {
      next(err);
    } else if (foundUser) {
      next(new Error('Email Already exists'));
    } else {
      next();
    }
  });
});

// hash password before saving user to db
UserSchema.pre('save', function (next) {
  this.password = bcrypt.hashSync(this.password, 10);
  next();
});

const User = db.model('User', UserSchema);

User.getUserById = (id) => User.findOne({ _id: id });

User.comparePassword = (userPassword, hashedPassword) => {
  return bcrypt.compare(userPassword, hashedPassword);
};

export default User;
