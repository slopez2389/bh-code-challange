import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.load();

const { SUPER_SECRET } = process.env;

export function createTokenWithUser(user) {
  if (user) {
    const payload = { user_id: user._id, user_role: user.role };
    return jwt.sign(payload, SUPER_SECRET, { expiresIn: 1440 });
  }
}

export function decodeToken(token) {
  return jwt.verify(token, SUPER_SECRET);
}

export function formatResponseError(message) {
  return {
    success: false,
    message,
  };
}
