export const ACCESS_TOKEN = 'access-token';

// API ENDPOINTS
export const LOGIN_ENDPOINT = '/login';
export const SIGN_UP_ENDPOINT = '/signup';
export const ME_ENDPOINT = '/me';
export const USERS_ENDPOINT = '/users';

// APP ROUTES
export const PROFILE_ROUTE = '/profile';

// MISC MESSAGES
export const LOGGED_OUT_MESSAGE = 'Please register and/or login and try again.';
