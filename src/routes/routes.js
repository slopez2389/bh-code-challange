import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from '../components/App/App';
import LoginContainer from '../components/Login/LoginContainer';
import LoggedInContainer from '../components/LoggedIn/LoggedInContainer';
import UsersContainer from '../components/Users/UsersContainer';

// logout user
function logout(nextState, replace, callback) {
  localStorage.clear();
  replace('/');
  callback();
}

export default (
  <Route path='/' component={App}>
    <IndexRoute component={LoginContainer} />
    <Route path='profile' component={LoggedInContainer} />
    <Route path='users' component={UsersContainer} />
    <Route path='logout' component={LoginContainer} onEnter={logout} />
  </Route>
);
