/* eslint-disable no-console */
const chalk = require('chalk');
const uuidv1 = require('uuid/v1');

module.exports = function (UserModel) {
  const generatedPassword = uuidv1();
  const email = 'admin@admin.com';
  const newUser = new UserModel({
    email,
    name: 'yolo',
    role: 'admin',
    password: generatedPassword,
  });

  newUser.save(newUser, (err) => {
    if (err) {
      console.log(`${chalk.red('error creating admin user')}`);
    }

    console.log(`
      ${chalk.green('admin user created')}
      email: ${chalk.blueBright(email)}
      password: ${chalk.blueBright(generatedPassword)}`);
  });
};
