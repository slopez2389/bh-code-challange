import React from 'react';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

class LoggedIn extends React.Component {
  render() {
    return (
      <div className='logged-in-box'>
        <div className='logged-in-box__content'>
          <p>Welcome {this.props.name}, <br /> Your email is {this.props.email}</p>
          <button onClick={() => browserHistory.push('/logout')}>Logout</button>
        </div>
      </div>
    );
  }
}

LoggedIn.propTypes = {
  name: PropTypes.string,
  email: PropTypes.string,
  success: PropTypes.bool,
  message: PropTypes.string,
};

export default LoggedIn;
