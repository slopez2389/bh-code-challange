import React from 'react';
import axios from 'axios';
import { ACCESS_TOKEN, ME_ENDPOINT } from '../../constants';
import LoggedIn from './LoggedIn';
import Authorization from '../Authorization/Authorization';

class LoggedInContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      success: false,
      message: null,
    };
  }

  componentDidMount() {
    const accessToken = localStorage.getItem(ACCESS_TOKEN);
    if (accessToken) {
      axios.post(ME_ENDPOINT, { accessToken })
        .then(({ data }) => {
          const {
            name, email, success,
          } = data;
          this.setState({ success, name, email });
        })
        .catch(({ response }) => {
          const {
            success, message,
          } = response.data;

          this.setState({ success, message });
        });
    }
  }

  render() {
    return (
      <Authorization>
        <LoggedIn {...this.state} />
      </Authorization>
    );
  }
}

export default LoggedInContainer;
