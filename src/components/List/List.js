import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

class List extends React.Component {
  render() {
    return (
      <div className='list'>
        {this.props.items.map((item) => (
          <ListItem
            key={item.header}
            header={item.header}
            description={item.description}
          />
        ))}
      </div>
    );
  }
}

List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    header: PropTypes.string,
    description: PropTypes.string,
  })),
};

export default List;
