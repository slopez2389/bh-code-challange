import React from 'react';
import PropTypes from 'prop-types';

function ListItem(props) {
  return (
    <div className='list__item'>
      <div className='list__item__body'>
        <div className='list__item__body__header'>{props.header}</div>
        <div className='list__item__body__description'>{props.description}</div>
      </div>
    </div>
  );
}

ListItem.propTypes = {
  header: PropTypes.string,
  description: PropTypes.string,
};

export default ListItem;
