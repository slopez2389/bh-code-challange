import React from 'react';
import axios from 'axios';
import { ACCESS_TOKEN, USERS_ENDPOINT } from '../../constants';
import Users from './Users';
import Authorization from '../Authorization/Authorization';

class UsersContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listItems: [],
      success: false,
    };
  }

  componentDidMount() {
    const accessToken = localStorage.getItem(ACCESS_TOKEN);

    if (accessToken) {
      axios.post(USERS_ENDPOINT, { accessToken })
        .then(({ data }) => {
          const listItems = data.users.map(this.formatListItemsFromUser);
          this.setState({ listItems });
        })
        .catch(({ response }) => {
          const isUnauthorized = response.status === 403;
          const {
            success, message,
          } = response.data;

          this.setState({ success, message, isUnauthorized });
        });
    }
  }

  formatListItemsFromUser = (user) => {
    return {
      header: user.name,
      description: user.email,
    };
  };

  render() {
    return (
      <Authorization>
        <Users {...this.state} />
      </Authorization>
    );
  }
}

export default UsersContainer;
