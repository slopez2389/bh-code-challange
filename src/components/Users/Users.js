import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import List from '../List/List';
import { PROFILE_ROUTE } from '../../constants';

class Users extends React.Component {
  render() {
    const {
      message,
      isUnauthorized,
    } = this.props;

    let component = null;

    if (isUnauthorized) {
      component = (
        <div>
          <p className='section-header'>{message}</p>
          <Link to={PROFILE_ROUTE}>Get me outta here</Link>
        </div>
      );
    } else if (this.props.listItems && this.props.listItems.length) {
      component = (
        <div>
          <h2 className='section-header'>Users</h2>
          <List items={this.props.listItems} />
        </div>
      );
    }

    return (
      <div>{component}</div>
    );
  }
}

Users.propTypes = {
  success: PropTypes.bool,
  message: PropTypes.string,
  listItems: PropTypes.arrayOf(PropTypes.shape({
    header: PropTypes.string,
    description: PropTypes.string,
  })),
  isUnauthorized: PropTypes.bool,
};

export default Users;
