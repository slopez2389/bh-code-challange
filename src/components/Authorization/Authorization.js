import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { ACCESS_TOKEN, LOGGED_OUT_MESSAGE } from '../../constants';

class Authorization extends React.Component {
  render() {
    const isLoggedIn = localStorage.getItem(ACCESS_TOKEN);

    if (isLoggedIn) {
      return (<div>{this.props.children}</div>);
    }

    return (
      <div className='authorization'>
        <p>{LOGGED_OUT_MESSAGE}</p>
        <Link to='/'>Go to Login</Link>
      </div>
    );
  }
}

Authorization.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Authorization;
