import 'flexboxgrid/css/flexboxgrid.min.css';
import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { ACCESS_TOKEN, PROFILE_ROUTE } from '../../constants';
import '../../scss/style.scss';

class App extends React.Component {
  storeUserToken = ({ token }) => {
    localStorage.setItem(ACCESS_TOKEN, token);
    browserHistory.push(PROFILE_ROUTE);
  };

  render() {
    return (
      <div className='row center-xs'>
        <div className='col-sm-6 col-xs-12'>
          {
            React.cloneElement(this.props.children, {
              storeUserToken: this.storeUserToken,
            })
          }
        </div>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element.isRequired,
};

export default App;
