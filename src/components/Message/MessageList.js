import React from 'react';
import PropTypes from 'prop-types';

function MessageList({ items }) {
  if (items && items.length) {
    return (
      <ul className='message__list'>
        {items.map((item) => {
          return <li className='message__list__item' key={item}>{item}</li>;
        })}
      </ul>
    );
  }
}

MessageList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default MessageList;
