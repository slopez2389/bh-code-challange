import React from 'react';
import PropTypes from 'prop-types';
import MessageList from './MessageList';

class Message extends React.Component {
  renderList = () => <MessageList items={this.props.list} />;

  render() {
    const colorClass = this.props.color && `message--${this.props.color}`;
    return (
      <div className={`message ${colorClass}`}>
        {this.props.header && <h5 className='message__header'>{this.props.header}</h5>}
        {this.renderList()}
        {this.props.content && <div className='message__content'>{this.props.content}</div>}
      </div>
    );
  }
}

Message.propTypes = {
  header: PropTypes.string,
  content: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.string),
  color: PropTypes.string,
};

Message.defaultProps = {
  color: '',
};

export default Message;
