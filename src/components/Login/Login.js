import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Message from '../Message/Message';
import { ACCESS_TOKEN } from '../../constants';

class Login extends React.Component {
  renderNameField = () => {
    return !this.props.login && (
      <div className='form__field'>
        <input
          name='name'
          type='text'
          placeholder='First and Last Name'
          value={this.props.name}
          onChange={this.props.handleOnChange}
        />
      </div>
    );
  };

  renderMessage = () => {
    return this.props.errors.length > 0 && (
      <Message
        color='error'
        list={this.props.errors}
      />
    );
  };

  renderForm = () => {
    return (
      <div>

        <h2 className='section-header'>{this.props.login ? 'Login' : 'Sign Up'}</h2>

        <form className='form' onSubmit={this.props.handleFormSubmit}>

          <div className='form__body'>

            {this.renderNameField()}

            <div className='form__field'>
              <input
                name='email'
                type='text'
                placeholder='Email'
                value={this.props.email}
                onChange={this.props.handleOnChange}
              />
            </div>

            <div className='form__field'>
              <input
                name='password'
                type='password'
                placeholder='Password'
                value={this.props.password}
                onChange={this.props.handleOnChange}
              />
            </div>

            <button className='form__submit' type='submit'>Submit</button>

            <button onClick={this.props.toggleForm} className='link'>
              {
                this.props.login ? 'Don\'t have an account ?' : 'Already have an account?'
              }
            </button>
          </div>

        </form>
        {this.renderMessage()}
      </div>
    );
  };

  render() {
    // only render form when user is logged out
    if (!localStorage.getItem(ACCESS_TOKEN)) {
      return (
        <div>{this.renderForm()}</div>
      );
    }

    return (
      <div>
        <p className='section-header'>Already logged in</p>
        <Link to='/profile'>View Profile</Link>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.bool,
  name: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.string),
  toggleForm: PropTypes.func,
  handleOnChange: PropTypes.func,
  handleFormSubmit: PropTypes.func,
  isLoggedIn: PropTypes.func,
};

export default Login;
