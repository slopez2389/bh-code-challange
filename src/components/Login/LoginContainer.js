import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { formatApiErrors } from '../../utils';
import { LOGIN_ENDPOINT, SIGN_UP_ENDPOINT } from '../../constants';
import Login from './Login';

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      login: true,
      errors: [],
      name: '',
      email: '',
      password: '',
    };
  }

  handleFormSubmit = (evt) => {
    evt.preventDefault();

    const {
      login,
      email,
      password,
      name,
    } = this.state;

    const endPoint = login ? LOGIN_ENDPOINT : SIGN_UP_ENDPOINT;

    axios.post(endPoint, { email, password, name })
      .then(({ data }) => this.props.storeUserToken(data))
      .catch(({ response }) => {
        const errors = formatApiErrors(response.data);
        this.setState({ errors });
      });
  };

  handleOnChange = (evt) => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  toggleForm = (evt) => {
    evt.preventDefault();
    this.setState({
      login: !this.state.login,
      name: '',
      email: '',
      password: '',
      errors: [],
    });
  };

  render() {
    return (
      <Login
        {...this.state}
        toggleForm={this.toggleForm}
        handleOnChange={this.handleOnChange}
        handleFormSubmit={this.handleFormSubmit}
      />
    );
  }
}

LoginContainer.propTypes = {
  storeUserToken: PropTypes.func,
};

export default LoginContainer;
