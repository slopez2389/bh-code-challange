export function formatApiErrors({ errors, message }) {
  const errorMessages = [];

  // mongoose validation errors
  if (errors) {
    return Object.keys((errors)).map((key) => {
      return errors[key].message;
    });
  } else if (message) {
    // api exception errors
    errorMessages.push(message);
  } else {
    errorMessages.push('Unknown Error!');
  }

  return errorMessages;
}
